package com.somospnt.democontract;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.somospnt.democontract.controller.PersonaRestController;
import org.junit.Before;

public class MvcTest {

    @Before
    public void setup() {
        RestAssuredMockMvc.standaloneSetup(new PersonaRestController());
    }

}
