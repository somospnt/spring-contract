org.springframework.cloud.contract.spec.Contract.make {
    request {
        method POST()
        url '/api/personas'
        headers {
            contentType(applicationJson())
        }
        body(
            nombre: $(regex('[a-zA-Z]+')),
            edad: $(regex('[0-9]+'))
        )
    }
    response {
        status 200
        body(
            id: $(regex('[0-9]+')),
            nombre: fromRequest().body('$.nombre'),
            edad: fromRequest().body('$.edad')
        )
    }
    priority 1
}