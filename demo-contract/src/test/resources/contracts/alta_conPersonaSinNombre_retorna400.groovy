org.springframework.cloud.contract.spec.Contract.make {
    request {
        method POST()
        url '/api/personas'
        headers {
            contentType(applicationJson())
        }
        body(
            edad: $(regex('[0-9]+'))
        )
    }
    response {
        status 400
    }
    priority 2
}