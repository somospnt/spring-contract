package com.somospnt.democontract.domain;

import lombok.Data;

@Data
public class Persona {

    private Long id;
    private String nombre;
    private int edad;

}
