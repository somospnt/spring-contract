package com.somospnt.democontract.controller;

import com.somospnt.democontract.domain.Persona;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaRestController {

    @RequestMapping(path = "/api/personas", method = RequestMethod.POST, consumes = "application/json")
    public Persona guardar(@RequestBody Persona persona) {
        Assert.notNull(persona.getNombre(), "Falta el nombre");
        persona.setId(2L);
        return persona;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void error(IllegalArgumentException exception) {

    }

}
