package com.somospnt.democontractrunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.contract.stubrunner.server.EnableStubRunnerServer;

@SpringBootApplication
@EnableStubRunnerServer
public class DemoContractRunnerApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoContractRunnerApplication.class, args);
	}
}
