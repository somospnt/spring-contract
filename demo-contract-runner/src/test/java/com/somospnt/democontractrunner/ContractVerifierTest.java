package com.somospnt.democontractrunner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import static org.assertj.core.api.Assertions.*;
import org.junit.Before;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
@AutoConfigureStubRunner(ids = {"com.somospnt:demo-contract:+:stubs:1000"}, workOffline = true)
public class ContractVerifierTest {

    private RestTemplate restTemplate;

    @Before
    public void setup() {
        restTemplate = new RestTemplate();
    }

    @Test
    public void altaPersona_conPersonaOk_retorna200() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity<String> response
                = restTemplate.exchange("http://localhost:" + 1000 + "/api/personas", HttpMethod.POST,
                        new HttpEntity<>("{\"nombre\":\"coco\",\"edad\":42}", headers),
                        String.class);
        assertThat(response.getStatusCodeValue()).isEqualTo(200);
        assertThat(response.getBody()).contains("\"id\":");
    }

    @Test(expected = HttpClientErrorException.class)
    public void altaPersona_conPersonaMal_retorna400() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            restTemplate.exchange("http://localhost:" + 1000 + "/api/personas", HttpMethod.POST,
                new HttpEntity<>("{\"edad\":42}", headers),
                String.class);
        } catch(HttpClientErrorException e) {
            assertThat(e.getStatusCode().value()).isEqualTo(400);
            throw e;
        }
        
    }

}
